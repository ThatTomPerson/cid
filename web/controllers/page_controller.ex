defmodule Cid.PageController do
  use Cid.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
